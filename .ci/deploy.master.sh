#!/usr/bin/env bash
set -x -e -o pipefail

# GitMate master deployment script
# This script is meant to be executed inside the master server

COMPOSE_LOCATION="/home/app/gitmate-2-frontend"
DOCKER_GROUP_NAME="docker"

# Early checks

which docker

which docker-compose

id -nG | grep -qw "$DOCKER_GROUP_NAME"

# Start deployment

cd $COMPOSE_LOCATION

docker stop gitmate-fe
docker container prune -f
docker image prune -a -f
docker run -d --restart unless-stopped -p 4200:80 --name gitmate-fe registry.gitlab.com/coala/gitmate/gitmate-2-frontend:latest
