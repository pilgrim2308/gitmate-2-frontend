export const environment = {
  production: true,
  backend_url: 'https://gitmateapi.abhishalya.tech',
  edition: 'enterprise',
};
