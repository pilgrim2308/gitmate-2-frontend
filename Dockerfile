FROM node:14-slim as builder
MAINTAINER Abhinav Kaushlya <abhinavkaushlya@gmail.com>

ENV ROOT=/app

ADD . $ROOT

WORKDIR $ROOT

RUN npm install

RUN npm rebuild node-sass

RUN npm run build:prod

FROM nginx:1.21.0-alpine

COPY --from=builder /app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
